output "web1_ip4" {
  value = digitalocean_droplet.web1.ipv4_address
}

output "web2_ip4" {
  value = digitalocean_droplet.web2.ipv4_address
}
