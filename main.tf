provider "digitalocean" {
  token = var.digitalocean_token
}

resource "digitalocean_droplet" "web1" {
  image = "debian-10-x64"
  name = "ejemplo-web1"
  region = "fra1"
  size = "s-1vcpu-1gb"
}

resource "digitalocean_droplet" "web2" {
  image = "debian-10-x64"
  name = "ejemplo-web2"
  region = "fra1"
  size = "s-1vcpu-1gb"
}

resource "digitalocean_droplet" "web3" {
  image = "debian-10-x64"
  name = "ejemplo-web3"
  region = "fra1"
  size = "s-1vcpu-1gb"
}

resource "digitalocean_project" "ejemplo_gitops" {
  name = "Ejemplo de GitOps"
  description = "Prueba de Terraform"
  environment = "Development"
  resources = [digitalocean_droplet.web1.urn, digitalocean_droplet.web2.urn, digitalocean_droplet.web3.urn]
}


